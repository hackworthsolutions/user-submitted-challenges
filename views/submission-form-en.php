<?php // User Submitted Posts - HTML5 Submission Form

global $usp_options, $current_user;

$author_ID  = $usp_options['author'];
$default_author = get_the_author_meta('display_name', $author_ID);
if ($authorName == $default_author) {
	$authorName = '';
}
if(is_user_logged_in()) {
  global $current_user;
  get_currentuserinfo();
}
?>

<!-- User Submitted Posts @ http://perishablepress.com/user-submitted-posts/ -->
<script type="text/javascript" src="/assets/js/vendor/jquery.validate.min.js"></script>
<div id="user-submitted-posts">

	<?php if ($usp_options['usp_form_content'] !== '') {
		echo $usp_options['usp_form_content'];
	} ?>

	<form method="post" id="challengeForm" enctype="multipart/form-data" action="">

		<?php if($_GET['submission-error'] == '1') { ?>
		<div id="usp-error-message"><?php echo $usp_options['error-message']; ?></div>
		<?php } ?>

		<?php if($_GET['success'] == '1') { ?>
		<div id="usp-success-message"><?php echo $usp_options['success-message']; ?></div>
		<?php } else { ?>

		<h3>Your Challenge</h3>
		<?php if ($usp_options['usp_title'] == 'show') { ?>
		<fieldset class="usp-title">
			<label for="user-submitted-title"><?php _e('Challenge Name'); ?></label>
			<input name="user-submitted-title" type="text" value="" placeholder="<?php _e('Challenge Name'); ?>" required maxlength="113">
		</fieldset>
		<?php } if (($usp_options['usp_category'] == 'show') && ($usp_options['usp_use_cat'] == false)) { ?>
		<fieldset class="usp-category">
			<label for="user-submitted-category"><?php _e('Line(s) of Work'); ?></label>
			<!-- <select name="user-submitted-category[]" multiple required> -->
			<div style="width:60%;float:left;">
				<?php foreach(array_merge($usp_options['categories'], array(19)) as $categoryId) { $category = get_term($categoryId, 'category'); if(!$category || $category->slug == "destacadas" || $category->slug == "retos") { continue; } ?>
				<!-- <option value="<?php echo $categoryId; ?>"><?php $category = get_term($categoryId, 'category'); echo htmlentities($category->name); ?></option>-->
				<input type="checkbox" name="user-submitted-category[]" style="width:auto;" value="<?php echo $categoryId; ?>">&nbsp;<?php $category = get_term($categoryId, 'category'); echo htmlentities($category->name); ?><br/>
				<?php } ?>
			<!-- </select> -->
			</div>
		</fieldset>
		<?php } if ($usp_options['usp_content'] == 'show') { ?>
		<fieldset class="usp-content">
			<label for="user-submitted-content"><?php _e('Challenge description'); ?></label>
			<textarea name="user-submitted-content" rows="5" placeholder="<?php _e('Describe your challenge'); ?>" maxlength=5000 required onfocus="$(this).html(''); $(this).off('focus')">
Tell us about your challenge:
- briefly describe the motivation behind the challenge
- add any other relevant information</textarea>
		</fieldset>
		<?php } if ($usp_options['usp_tags'] == 'show') { ?>
		<fieldset class="usp-tags">
			<label for="user-submitted-tags"><?php _e('Tags (optional)'); ?></label>
			<input name="user-submitted-tags" id="user-submitted-tags" type="text" value="" placeholder="<?php _e('Separated by commas'); ?>">
		</fieldset>
		<?php } ?>
		<?php if ($usp_options['usp_images'] == 'show') { ?>
		<?php if ($usp_options['max-images'] !== 0) { ?>
		<h4>Media (optional, but highly recommended)</h4>
                <fieldset class="usp-twitter">
                        <label for="user-submitted-twitter"><?php _e('Twitter'); ?></label>
			<div>
			  <input type="radio" checked="checked" style="float:none; width:auto;" name="user-submitted-twitter" value="1">Yes, spread the word! &nbsp; <input type="radio" style="float:none; width:auto;" name="user-submitted-twitter" value="0">No, I&#39;d like to buzz it myself
			</div>
                </fieldset>
                <fieldset class="usp-video">
                        <label for="user-submitted-video"><?php _e('Video'); ?></label>
                        <input name="user-submitted-video" type="url" value="" placeholder="<?php _e('http://youtube.com/watch?v=my-video'); ?>">
                </fieldset>
		<fieldset class="usp-images">
			<label for="user-submitted-image">Image</label>
				<?php if($usp_options['min-images'] < 1) {
					$numberImages = 1;
				} else {
					$numberImages = $usp_options['min-images'];
				} for($i = 0; $i < $numberImages; $i++) { ?>
				<input name="user-submitted-image[]" type="file" size="25">
				<?php } ?>
				<!-- <a href="#" id="usp_add-another"><?php _e('Add another image'); ?></a> -->
		</fieldset>
		<?php } ?>
		<?php } ?>

		<hr/>

		<h3>You</h3>

		<fieldset class="usp-anonymous">
		<label for="user-submitted-anonymous"><?php _e('Anonymous?'); ?></label>
		<div>
		  <input type="radio" checked="checked" style="width:auto;" name="user-submitted-anonymous" value="1">No, submit with my name<br>
		  <input type="radio" style="width:auto;" name="user-submitted-anonymous" value="0">Yes, I&#39;d like to submit anonymously
		</div>
		</fieldset>	
		<script>
		  $('input[name=user-submitted-anonymous]').on('change', function() {
		    if($(this).val() == '0') {
		      $('input[name=user-submitted-name]').val('Anonymous');
		      $('input[name=user-submitted-email]').val('hackforgood.net+anonymous@gmail.com');
		      $('input[name=user-submitted-url]').val('http://hackforgood.net/category/retos/');
		      $('#personal_info_challenge').hide();
		    } else {
		      $('input[name=user-submitted-name]').val('<?php echo (is_user_logged_in() ? $current_user->display_name : ""); ?>');
		      $('input[name=user-submitted-email]').val('<?php echo (is_user_logged_in() ? $current_user->user_email : ""); ?>');
		      $('input[name=user-submitted-url]').val('<?php echo (is_user_logged_in() ? $current_user->user_url : ""); ?>');
		      $('#personal_info_challenge').show();
		    }
		  });
		</script>

		<div id="personal_info_challenge">
		    <?php if(is_user_logged_in()) { ?>
			<?php if (($usp_options['usp_name'] == 'show') && ($usp_options['usp_use_author'] == false)) { ?>
			<fieldset class="usp-name">
				<label for="user-submitted-name"><?php _e('Your Name'); ?></label>
				<input name="user-submitted-name" type="text" value="<?php echo $current_user->display_name; ?>" readonly>
			</fieldset>
			<fieldset class="usp-email">
				<label for="user-submitted-email"><?php _e('Your E-mail'); ?></label>
				<input name="user-submitted-email" type="email" value="<?php echo $current_user->user_email; ?>" readonly>
			</fieldset>
			<?php } if (($usp_options['usp_url'] == 'show') && ($usp_options['usp_use_url'] == false)) { ?>
			<fieldset class="usp-url">
				<label for="user-submitted-url"><?php _e('Your URL (optional)'); ?></label>
				<input name="user-submitted-url" type="url" value="<?php echo $current_user->user_url; ?>" readonly>
			</fieldset>
			<?php } ?>
			<fieldset>
				<label for="user-submitted-edit-profile-link">Wanna change this?</label>
				<a href="<?php echo admin_url( 'profile.php' ); ?>">Edit your profile</a> - 
				<a href="<?php echo wp_logout_url( get_permalink() ); ?>" title="Logout">Logout</a>
			</fieldset>
		    <?php } else { ?>
			<?php if (($usp_options['usp_name'] == 'show') && ($usp_options['usp_use_author'] == false)) { ?>
			<div class="alert alert-success">
		               	<button type="button" class="close" data-dismiss="alert">&times;</button>
		                <strong>Notice</strong>: your contact information won't be made public or shared with any third party, though the organization may contact you regarding your challenge.
                	</div>
			<fieldset class="usp-name">
				<label for="user-submitted-name"><?php _e('Your Name'); ?></label>
				<input name="user-submitted-name" type="text" value="" placeholder="<?php _e('Your Name'); ?>" required minlength="4">
			</fieldset>
			<fieldset class="usp-email">
				<label for="user-submitted-email"><?php _e('Your E-mail'); ?></label>
				<input name="user-submitted-email" type="email" value="" placeholder="<?php _e('Your E-mail'); ?>" required>
			</fieldset>
			<?php } if (($usp_options['usp_url'] == 'show') && ($usp_options['usp_use_url'] == false)) { ?>
			<fieldset class="usp-url">
				<label for="user-submitted-url"><?php _e('Your URL'); ?></label>
				<input name="user-submitted-url" type="url" value="" placeholder="<?php _e('Your URL'); ?>">
			</fieldset>
			<?php } ?>
		    <?php } ?>

		    <hr/>
		</div>

		<h3>Captcha</h3>
		<?php if ($usp_options['usp_captcha'] == 'show') { ?>
		<fieldset class="usp-captcha">
			<label for="user-submitted-captcha"><?php echo $usp_options['usp_question']; ?></label>
			<input name="user-submitted-captcha" type="text" value="" placeholder="Answer to prove you are human" required>
		</fieldset>
		<?php } ?>
		<fieldset id="coldform_verify" style="display:none;">
			<label for="user-submitted-verify">Human verification: leave this field empty.</label>
			<input name="user-submitted-verify" type="text" value="">
		</fieldset>
		<div id="usp-submit">
			<?php if (!empty($usp_options['redirect-url'])) { ?>
			<input type="hidden" name="redirect-override" value="<?php echo $usp_options['redirect-url']; ?>">
			<?php } ?>
			<?php if ($usp_options['usp_use_author'] == true) { ?>
			<input class="hidden" type="hidden" name="user-submitted-name" value="<?php echo $current_user->user_login; ?>">
			<?php } ?>
			<?php if ($usp_options['usp_use_url'] == true) { ?>
			<input class="hidden" type="hidden" name="user-submitted-url" value="<?php echo $current_user->user_url; ?>">
			<?php } ?>
			<?php if ($usp_options['usp_use_cat'] == true) { ?>
			<input class="hidden" type="hidden" name="user-submitted-category" value="<?php echo $usp_options['usp_use_cat_id']; ?>">
			<?php } ?>
			<input name="user-submitted-post" class="btn btn-primary" type="submit" value="<?php _e('Submit Challenge'); ?>">
		</div>

		<?php } ?>

	</form>
</div>
<script>
  $("#challengeForm").validate();
  (function(){var e = document.getElementById("coldform_verify");e.parentNode.removeChild(e);})();
</script>
<!-- User Submitted Posts @ http://perishablepress.com/user-submitted-posts/ -->
