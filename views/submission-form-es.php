<?php // User Submitted Posts - HTML5 Submission Form

global $usp_options, $current_user;

$author_ID  = $usp_options['author'];
$default_author = get_the_author_meta('display_name', $author_ID);
if ($authorName == $default_author) {
	$authorName = '';
} ?>

<!-- User Submitted Posts @ http://perishablepress.com/user-submitted-posts/ -->
<script type="text/javascript" src="/assets/js/vendor/jquery.validate.min.js"></script>
<div id="user-submitted-posts">

	<?php if ($usp_options['usp_form_content'] !== '') {
		echo $usp_options['usp_form_content'];
	} ?>

	<script>
		function validateForm() {
			var valid = $('div.checkbox-group.required :checkbox:checked').length > 0;
			if(!valid) {
				alert('Necesitas incluir alguna categoría');
			}
			return valid;
		}
	</script>

	<form method="post" id="challengeForm" enctype="multipart/form-data" action="" onsubmit="return validateForm()">

		<?php if($_GET['submission-error'] == '1') { ?>
		<div id="usp-error-message" class="alert alert-danger" role="alert">Se ha producido un error. Por favor comprueba que has rellenado todos los campos.</div>
		<?php } ?>

		<?php if($_GET['success'] == '1') { ?>
		<div id="usp-success-message" class="alert alert-success" role="alert">El reto se ha enviado con éxito. Gracias por tu colaboración.</div>
		<?php } else { ?>

		<h3>Tu Reto</h3>
		<?php if ($usp_options['usp_title'] == 'show') { ?>
		<fieldset class="usp-title">
			<label for="user-submitted-title"><?php _e('Nombre del reto (Obligatorio)'); ?></label>
			<input name="user-submitted-title" type="text" value="" placeholder="<?php _e('Nombre del reto'); ?>" required maxlength="113">
		</fieldset>
		<?php } if (($usp_options['usp_category'] == 'show') && ($usp_options['usp_use_cat'] == false)) { ?>
		<fieldset class="usp-category">
			<label for="user-submitted-category"><?php _e('Línea(s) temática(s) (Obligatorio)'); ?></label>
			<!-- <select name="user-submitted-category[]" multiple required> -->
			<div style="width:60%;float:left;" class="checkbox-group required">
				<?php foreach(array_merge($usp_options['categories'], array(19)) as $categoryId) { $category = get_term($categoryId, 'category'); if(!$category || $category->slug == "destacadas" || $category->slug == "retos") { continue; } ?>
				<!-- <option value="<?php echo $categoryId; ?>"><?php $category = get_term($categoryId, 'category'); echo htmlentities($category->name); ?></option>-->
				<input type="checkbox" name="user-submitted-category[]" style="width:auto;" value="<?php echo $categoryId; ?>">&nbsp;<?php $category = get_term($categoryId, 'category'); echo htmlentities($category->name); ?><br/>
				<?php } ?>
				<input type="checkbox" name="user-submitted-category[]" style="width:auto;" value="31">&nbsp;Otras<br/>
			<!-- </select> -->

				<?php if ($usp_options['usp_tags'] == 'show') { ?>
				<fieldset class="usp-tags">
					<input name="user-submitted-tags" id="user-submitted-tags" type="text" value="" placeholder="<?php _e('Separadas por comas'); ?>">
				</fieldset>
				<?php } ?>
			</div>
		</fieldset>
		<?php } if ($usp_options['usp_content'] == 'show') { ?>
		<fieldset class="usp-content">
			<label for="user-submitted-content"><?php _e('Descripción del reto (Obligatorio)'); ?></label>
			<textarea name="user-submitted-content" rows="5" placeholder="<?php _e('¿En qué consiste tu reto?'); ?>" maxlength=5000 required onfocus="$(this).html(''); $(this).off('focus')"></textarea>
		</fieldset>
		<?php } if ($usp_options['usp_images'] == 'show') { ?>
		<?php if ($usp_options['max-images'] !== 0) { ?>
		<h4>Media (opcional, pero recomendado)</h4>
                <fieldset class="usp-twitter">
                        <label for="user-submitted-twitter"><?php _e('Twitter'); ?></label>
			<div>
			  <input type="radio" checked="checked" style="float:none; width:auto;" name="user-submitted-twitter" value="1">Sí! Dadle difusión &nbsp; <input type="radio" style="float:none; width:auto;" name="user-submitted-twitter" value="0">No, prefiero encargarme de su promoción por mi cuenta
			</div>
                </fieldset>
                <fieldset class="usp-video">
                        <label for="user-submitted-video"><?php _e('Vídeo'); ?></label>
                        <input name="user-submitted-video" type="url" value="" placeholder="<?php _e('http://youtube.com/watch?v=my-video'); ?>">
                </fieldset>
		<fieldset class="usp-images">
			<label for="user-submitted-image">Imagen</label>
				<?php if($usp_options['min-images'] < 1) {
					$numberImages = 1;
				} else {
					$numberImages = $usp_options['min-images'];
				} for($i = 0; $i < $numberImages; $i++) { ?>
				<input name="user-submitted-image[]" type="file" size="25">
				<?php } ?>
				<!-- <a href="#" id="usp_add-another"><?php _e('Add another image'); ?></a> -->
		</fieldset>
		<?php } ?>
		<?php } ?>

		<hr/>

		<h3>Datos sobre ti (Opcional)</h3>

                <fieldset class="usp-anonymous">
                <label for="user-submitted-anonymous">&iquest;An&oacute;nimo?</label>
                <div>
                  <input type="radio" checked="checked" style="width:auto;" name="user-submitted-anonymous" value="1">No, envia con mi nombre<br>
                  <input type="radio" style="width:auto;" name="user-submitted-anonymous" value="0">S&iacute;, quiero enviarlo an&oacute;nimamente
                </div>
                </fieldset>
                <script>
                  $('input[name=user-submitted-anonymous]').on('change', function() {
                    if($(this).val() == '0') {
                      $('input[name=user-submitted-name]').val('Anonymous');
                      $('input[name=user-submitted-email]').val('hackforgood.net+anonymous@gmail.com');
                      $('input[name=user-submitted-url]').val('http://hackforgood.net/category/retos/');
                      $('#personal_info_challenge').hide();
                    } else {
                      $('input[name=user-submitted-name]').val('<?php echo (is_user_logged_in() ? $current_user->display_name : ""); ?>');
                      $('input[name=user-submitted-email]').val('<?php echo (is_user_logged_in() ? $current_user->user_email : ""); ?>');
                      $('input[name=user-submitted-url]').val('<?php echo (is_user_logged_in() ? $current_user->user_url : ""); ?>');
                      $('#personal_info_challenge').show();
                    }
                  });
                </script>

		<div id="personal_info_challenge">
		    <?php if(is_user_logged_in()) { ?>
			<?php global $current_user;
			      get_currentuserinfo(); ?>
			<?php if (($usp_options['usp_name'] == 'show') && ($usp_options['usp_use_author'] == false)) { ?>
			<fieldset class="usp-name">
				<label for="user-submitted-name"><?php _e('Nombre'); ?></label>
				<input name="user-submitted-name" type="text" value="<?php echo $current_user->display_name; ?>" readonly>
			</fieldset>
			<fieldset class="usp-email">
				<label for="user-submitted-email"><?php _e('Correo electrónico'); ?></label>
				<input name="user-submitted-email" type="email" value="<?php echo $current_user->user_email; ?>" readonly>
			</fieldset>
			<?php } if (($usp_options['usp_url'] == 'show') && ($usp_options['usp_use_url'] == false)) { ?>
			<fieldset class="usp-url">
				<label for="user-submitted-url"><?php _e('URL de tu blog (opcional)'); ?></label>
				<input name="user-submitted-url" type="url" value="<?php echo $current_user->user_url; ?>" readonly>
			</fieldset>
			<?php } ?>
			<fieldset>
				<label for="user-submitted-edit-profile-link">¿Quieres modificarlo?</label>
				<a href="<?php echo admin_url( 'profile.php' ); ?>">Edita tu perfil</a> - 
				<a href="<?php echo wp_logout_url( get_permalink() ); ?>" title="Salir">Salir</a>
			</fieldset>
		    <?php } else { ?>
			<?php if (($usp_options['usp_name'] == 'show') && ($usp_options['usp_use_author'] == false)) { ?>
			<div class="alert alert-success">
		               	<button type="button" class="close" data-dismiss="alert">&times;</button>
		                <strong>Nota</strong>: Tu email no se hará público ni será compartido con terceras partes. La organización lo utilizará únicamente para contactarte respecto al reto.
                	</div>
			<fieldset class="usp-name">
				<label for="user-submitted-name"><?php _e('Nombre'); ?></label>
				<input name="user-submitted-name" type="text" value="" placeholder="<?php _e('Nombre'); ?>" required minlength="4">
			</fieldset>
			<fieldset class="usp-email">
				<label for="user-submitted-email"><?php _e('Correo electrónico'); ?></label>
				<input name="user-submitted-email" type="email" value="" placeholder="<?php _e('Correo electrónico'); ?>" required>
			</fieldset>
			<?php } if (($usp_options['usp_url'] == 'show') && ($usp_options['usp_use_url'] == false)) { ?>
			<fieldset class="usp-url">
				<label for="user-submitted-url"><?php _e('URL de tu blog'); ?></label>
				<input name="user-submitted-url" type="url" value="" placeholder="<?php _e('URL de tu blog'); ?>">
			</fieldset>
			<?php } ?>
		    <?php } ?>

		    <hr/>
		</div>

		<?php if ($usp_options['usp_captcha'] == 'show') { ?>
		<h3>Captcha</h3>
		<fieldset class="usp-captcha">
			<label for="user-submitted-captcha"><?php echo $usp_options['usp_question']; ?></label>
			<input name="user-submitted-captcha" type="text" value="" placeholder="Demuestra que eres humano" required>
		</fieldset>
		<?php } ?>
		<fieldset id="coldform_verify" style="display:none;">
			<label for="user-submitted-verify">Comprobación: deja este campo vacío.</label>
			<input name="user-submitted-verify" type="text" value="">
		</fieldset>
		<div id="usp-submit">
			<?php if (!empty($usp_options['redirect-url'])) { ?>
			<input type="hidden" name="redirect-override" value="<?php echo $usp_options['redirect-url']; ?>">
			<?php } ?>
			<?php if ($usp_options['usp_use_author'] == true) { ?>
			<input class="hidden" type="hidden" name="user-submitted-name" value="<?php echo $current_user->user_login; ?>">
			<?php } ?>
			<?php if ($usp_options['usp_use_url'] == true) { ?>
			<input class="hidden" type="hidden" name="user-submitted-url" value="<?php echo $current_user->user_url; ?>">
			<?php } ?>
			<?php if ($usp_options['usp_use_cat'] == true) { ?>
			<input class="hidden" type="hidden" name="user-submitted-category" value="<?php echo $usp_options['usp_use_cat_id']; ?>">
			<?php } ?>
			<input name="user-submitted-post" class="btn btn-primary" type="submit" value="<?php _e('Enviar reto'); ?>">
		</div>
		<?php } ?>

	</form>
</div>
<script>
  $("#challengeForm").validate();
  (function(){var e = document.getElementById("coldform_verify");e.parentNode.removeChild(e);})();
</script>
<!-- User Submitted Posts @ http://perishablepress.com/user-submitted-posts/ -->
